import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class Constants {

    private static String chromeWebDriver = "webdriver.chrome.driver";
    private static String chromeWebDriverPath = "src\\main\\resources\\chromedriver.exe";
    static String http = "http://localhost:3000";

    static WebDriver getDriver(String browser) {

        switch (browser.toLowerCase()) {
            case "chrome":
                System.setProperty(chromeWebDriver,chromeWebDriverPath);
                WebDriver driver = new ChromeDriver();
                return driver;
            default:
                System.out.println("Choose a valid Browser to Test");
                System.exit(0);
        }
        return null;
    }
}
