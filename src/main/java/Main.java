public class Main {

    public static void main(String[] args) {

        Sorting sorting = new SortingSelenium();
        sorting.setup();
        sorting.startSort();
    }
}
