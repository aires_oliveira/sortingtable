import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

class SortingSelenium implements Sorting{

    private WebDriver driver;
    private Actions act;

    @Override
    public void setup() {
        driver = Constants.getDriver("chrome");
        act = new Actions(driver);
        driver.get(Constants.http);
    }

    @Override
    public void startSort() {
        int maxY = getMaxY(driver);
        sortItems(driver,act,maxY);
    }

    private int getMaxY(WebDriver driver) {
        int maxY = 0;

        for (int i = 0; i < 6; i++) {
            WebElement element = driver.findElement(By.xpath("//li[contains(text(), 'Item " + i + "')]"));
            int y = element.getLocation().y;
            if (maxY < y) {
                maxY = y;
            }
        }
        return maxY;
    }


    private void sortItems(WebDriver driver, Actions act, int maxY) {
        for (int i = 0; i < 6; i++) {
            WebElement element = driver.findElement(By.xpath("//li[contains(text(), 'Item " + i + "')]"));
            act.dragAndDropBy(element, element.getLocation().x, maxY).build().perform();
        }
    }

}

